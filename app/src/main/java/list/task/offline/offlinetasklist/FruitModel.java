package list.task.offline.offlinetasklist;

/**
 * Created by Akash Jagtap on 7/24/2017.
 */

public class FruitModel {
    private String fruitName;
    private int fruitId;
    public FruitModel(int fruitId,String fruitName)
    {
        this.fruitName=fruitName;
        this.fruitId=fruitId;
    }

    public int getFruitId() {
        return fruitId;
    }

    public String getFruitName() {
        return fruitName;
    }
}
