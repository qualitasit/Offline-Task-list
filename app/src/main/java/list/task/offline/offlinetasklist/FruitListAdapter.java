package list.task.offline.offlinetasklist;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;


import java.util.ArrayList;

/**
 * Created by Akash on 2015-08-24.
 */
public class FruitListAdapter extends RecyclerView.Adapter<FruitListAdapter.ViewHolder> {
    private ArrayList<FruitModel> list;
    OnItemClickListener mItemClickListener;

    // Provide a suitable constructor (depends on the kind of dataset)
    public FruitListAdapter(ArrayList<FruitModel> myDataset) {
        list = myDataset;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent,
                                         int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item, parent, false);

        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {


        FruitModel mObjFruitModel=list.get(position);
        holder.txt_fruit.setText(mObjFruitModel.getFruitName());
        holder.layView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mItemClickListener != null) {
                    mItemClickListener.onCellClick(view, position);
                }
            }
        });

    }


    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        protected TextView txt_fruit;

        protected View layView;

        public ViewHolder(View itemView) {
            super(itemView);
            txt_fruit = (TextView) itemView.findViewById(R.id.txt_fruit);
            layView = itemView;


        }


    }

    public interface OnItemClickListener {
        public void onItemClick(View view, int position);
        public void onCellClick(View view, int position);
    }

    public void SetOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }

}