package list.task.offline.offlinetasklist;

import android.app.Application;
import android.content.Context;


public class AppController extends Application {

	public static final String TAG = AppController.class.getSimpleName();
	private static AppController mInstance;
	private static Context context;

	@Override
	public void onCreate() {
		super.onCreate();
		mInstance = this;
		context = getApplicationContext();


	}

	public static Context getAppContext() {
		return context;
	}
	/*private AppController(){}*/
	public static synchronized AppController getInstance() {
		return mInstance;
	}


}