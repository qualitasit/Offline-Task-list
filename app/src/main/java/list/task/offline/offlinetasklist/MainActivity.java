package list.task.offline.offlinetasklist;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Iterator;

import javax.net.ssl.HttpsURLConnection;

public class MainActivity extends AppCompatActivity {

    FruitListAdapter mObjFruitListAdapter;
    ArrayList<FruitModel> mArrFruitModel = new ArrayList<>();
    SQLiteHandler dbObject;
    EditText enter_fruit;
    ArrayList<FruitModel> mArrFruitModelMissedItem = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        dbObject = new SQLiteHandler(this);
        mArrFruitModel = new ArrayList<>();
        mArrFruitModel = dbObject.geFruitData();
        mArrFruitModelMissedItem = dbObject.geFruitUnSycItem(CompanyPreferences.getInstance().getFruitSynId());

        if (mArrFruitModelMissedItem.size() > 0) {
            saveToServer(mArrFruitModelMissedItem);
        }
        RecyclerView recHeaderList = (RecyclerView) findViewById(R.id.list);
        enter_fruit = (EditText) findViewById(R.id.enter_fruit);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        mObjFruitListAdapter = new FruitListAdapter(mArrFruitModel);
        recHeaderList.setLayoutManager(layoutManager);
        recHeaderList.setAdapter(mObjFruitListAdapter);
    }

    public void addFruit(View view) {
        try {


            if (dbObject.addFruit(enter_fruit.getText().toString()) > 0) {
                int FruitId = 1;
                if (mArrFruitModel.size() > 1) {
                    FruitId = mArrFruitModel.get(mArrFruitModel.size() - 1).getFruitId() + 1;
                }
                mArrFruitModel.add(new FruitModel(FruitId,
                        enter_fruit.getText().toString()));
                if (Internet.isNetworkAvailable(this)) {
                    ArrayList<FruitModel> saveData = new ArrayList<>();
                    saveData.add(mArrFruitModel.get(mArrFruitModel.size() - 1));
                    saveToServer(saveData);
                } else {
                    //Toast.makeText(getApplicationContext(), "NO Internet Connection!", Toast.LENGTH_SHORT).show();
                    Toast.makeText(this, "No Internet Connection! No Internet Connection!", Toast.LENGTH_SHORT).show();
                }
            }

        } catch (Exception ex) {

        }
    }


    private void saveToServer(final ArrayList<FruitModel> mArrFruitModelMissedItem) {
        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
                URL githubEndpoint = null;
                int finalIdex = 1;
                try {
                    githubEndpoint = new URL("https://api.github.com/");

                    JSONObject postDataParams = new JSONObject();
                    for (FruitModel obj : mArrFruitModelMissedItem) {
                        postDataParams.put("fruitId", obj.getFruitId());
                        postDataParams.put("fruitName", obj.getFruitName());
                        finalIdex = obj.getFruitId();
                    }

                    HttpURLConnection conn = (HttpURLConnection) githubEndpoint.openConnection();
                    conn.setReadTimeout(15000);
                    conn.setConnectTimeout(15000);
                    conn.setRequestMethod("POST");
                    conn.setDoInput(true);
                    conn.setDoOutput(true);

                    OutputStream os = conn.getOutputStream();
                    BufferedWriter writer = new BufferedWriter(
                            new OutputStreamWriter(os, "UTF-8"));
                    writer.write(getPostDataString(postDataParams));

                    writer.flush();
                    writer.close();
                    os.close();
                    int responseCode = conn.getResponseCode();

                    if (responseCode == HttpsURLConnection.HTTP_OK) {
                        CompanyPreferences.getInstance().setFruitSynId(finalIdex);

                    }


                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });
    }

    public String getPostDataString(JSONObject params) throws Exception {

        StringBuilder result = new StringBuilder();
        boolean first = true;

        Iterator<String> itr = params.keys();

        while (itr.hasNext()) {

            String key = itr.next();
            Object value = params.get(key);

            if (first)
                first = false;
            else
                result.append("&");

            result.append(URLEncoder.encode(key, "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(value.toString(), "UTF-8"));

        }
        return result.toString();
    }
}
