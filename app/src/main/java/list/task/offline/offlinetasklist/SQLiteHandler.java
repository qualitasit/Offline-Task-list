/**
 * Author: Ravi Tamada
 * URL: www.androidhive.info
 * twitter: http://twitter.com/ravitamada
 */
package list.task.offline.offlinetasklist;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;


import java.util.ArrayList;

public class SQLiteHandler extends SQLiteOpenHelper {

    private static final String TAG = SQLiteHandler.class.getSimpleName();

    private static final int DATABASE_VERSION = 1;

    // Database Name
    private static final String DATABASE_NAME = "android_api";

    // Login table name
    private static final String TABLE_FRUIT = "Fruit";

    // Login Table Columns names
    private static final String KEY_ID = "id";
    private static final String KEY_FRUIT_NAME = "fruit_name";


    public SQLiteHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    // Creating Tables
    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_FRUIT_TABLE = "CREATE TABLE " + TABLE_FRUIT + "("
                + KEY_ID + " INTEGER PRIMARY KEY," + KEY_FRUIT_NAME + " TEXT " + ")";


        db.execSQL(CREATE_FRUIT_TABLE);

        Log.d(TAG, "Database tables created");
    }

    // Upgrading database
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_FRUIT);
        // create new tables
        onCreate(db);
    }


    public long addFruit(String fruitname) {
        SQLiteDatabase db = this.getWritableDatabase();
        long id=0;
        try {
            ContentValues values = new ContentValues();
            values.put(KEY_FRUIT_NAME, fruitname);
             id = db.insert(TABLE_FRUIT, null, values);

        } catch (Exception ex) {

        } finally {
            db.close(); // Closing database connection
        }
         return id;

    }

    public ArrayList<FruitModel> geFruitData() {
        ArrayList<FruitModel> mArrFruitModel = new ArrayList<>();
        try {
            String selectQuery = "SELECT  * FROM " + TABLE_FRUIT;


            SQLiteDatabase db = this.getReadableDatabase();

            Cursor cursor = db.rawQuery(selectQuery, null);

            if (cursor.moveToFirst()) {

                do {
                    mArrFruitModel.add(new FruitModel(cursor.getInt(0), cursor.getString(1)));
                } while (cursor.moveToNext());

            }
            cursor.close();
            db.close();

        } catch (Exception ex) {
            mArrFruitModel.clear();
            String str = ex + "";
        }

        return mArrFruitModel;
    }

    public ArrayList<FruitModel> geFruitUnSycItem(int fruitSynId) {
        ArrayList<FruitModel> mArrFruitModel = new ArrayList<>();
        try {
            String selectQuery = "SELECT  * FROM " + TABLE_FRUIT +"where "+TABLE_FRUIT+">"+fruitSynId;


            SQLiteDatabase db = this.getReadableDatabase();

            Cursor cursor = db.rawQuery(selectQuery, null);

            if (cursor.moveToFirst()) {

                do {
                    mArrFruitModel.add(new FruitModel(cursor.getInt(0), cursor.getString(1)));
                } while (cursor.moveToNext());

            }
            cursor.close();
            db.close();

        } catch (Exception ex) {
            mArrFruitModel.clear();
            String str = ex + "";
        }

        return mArrFruitModel;
    }
/*
    public void addTempUser(ArrayList<String[]> object) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("delete from " + TABLE_EMPOLYEE_CALENDAR);
        db.beginTransactionNonExclusive();
        try {
            int i = 0;
            while (object.size() > i) {
                ContentValues values = new ContentValues();

                String[] str = object.get(i);
                values.put(KEY_DATE_TYPE, str[0]);
                values.put(KEY_DATE, str[1]);
                values.put(KEY_IS_HALFDAY, str[2]);
                long id = db.insert(TABLE_EMPOLYEE_CALENDAR, null, values);
                i++;
            }
            db.setTransactionSuccessful();
        } catch (Exception ex) {
            String str = ex + "";
        } finally {
            db.endTransaction();
            db.close(); // Closing database connection
        }


    }

    public ArrayList<CalendarDay> getCalendarData(boolean EmployeeCal, String datetype, String halfDay) {

        String selectQuery = "";
        if (EmployeeCal)
            selectQuery = "SELECT  * FROM " + TABLE_EMPOLYEE_CALENDAR + " Where " + KEY_DATE_TYPE + "='" + datetype + "' and " + KEY_IS_HALFDAY + "='" + halfDay + "'";
        else
            selectQuery = "SELECT  * FROM " + TABLE_CALENDAR + " Where " + KEY_DATE_TYPE + "='" + datetype + "' and " + KEY_IS_HALFDAY + "='" + halfDay + "'";
        ArrayList<CalendarDay> mobjreturn = new ArrayList<>();
        ArrayList<String[]> objreturn = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        try {
            Cursor cursor = db.rawQuery(selectQuery, null);
            // Move to first row
            //cursor.moveToFirst();
            if (cursor.moveToFirst()) {

                do {
                    CalendarDay day = CalendarDay.from(cursor.getString(1), "dd/MM/yyyy");
                    mobjreturn.add(day);
                } while (cursor.moveToNext());

            }
            cursor.close();
            db.close();

        } catch (Exception ex) {
            String str = ex + "";
        }

        return mobjreturn;
    }

    public void addEmployees(ArrayList<EmployeeInfo> object) {
        SQLiteDatabase db = this.getWritableDatabase();

        db.beginTransactionNonExclusive();
        // db.execSQL("delete from "+ TABLE_EMPLOYEE_LIST);
        try {
            int i = 0;
            while (object.size() > i) {
                ContentValues values = new ContentValues();

                EmployeeInfo empObj = object.get(i);
                values.put(KEY_EMP_FIRST_NAME, empObj.getEmpFristName());
                values.put(KEY_EMP_LAST_NAME, empObj.getEmpLastName());
                values.put(KEY_EMP_NO, empObj.getEmpEmpNO());
                values.put(KEY_EMP_MARGNO, empObj.getEmpMrgNo());
                values.put(KEY_IS_MRG, empObj.getEmpIsMgr());
                long id = db.insert(TABLE_EMPLOYEE_LIST, null, values);
                i++;
            }
            db.setTransactionSuccessful();
        } catch (Exception ex) {

        } finally {
            db.endTransaction();
            db.close(); // Closing database connection
        }


    }

    public void deleteEmployees() {
        try {


            SQLiteDatabase db = this.getWritableDatabase();
            db.execSQL("delete from " + TABLE_EMPLOYEE_LIST);
            String selectQuery = "SELECT  * FROM " + TABLE_EMPLOYEE_LIST;
            Cursor cursor = db.rawQuery(selectQuery, null);
            int size = cursor.getCount();
            db.close();
        } catch (Exception ex) {

        }
    }

    public ArrayList<EmployeeInfo> getEmployee(String mrgNo) {

       *//* private static final String KEY_EMP_NO = "emp_no";
        private static final String KEY_EMP_MARGNO = "emp_mrg";
        private static final String KEY_IS_MRG = "is_mrg";*//*

        boolean getPrefDirectList = SessionManager.getInstance().getShowDirect();
        boolean getsortBy = SessionManager.getInstance().getSortByFirstNameValue();

        String sortBy = "first_name";
        if (getsortBy)
            sortBy = "last_name";
        String selectQuery = "";
        if (getPrefDirectList)
            selectQuery = "SELECT  * FROM " + TABLE_EMPLOYEE_LIST + " Where " + KEY_EMP_MARGNO + "='" + mrgNo + "' order by " + sortBy;
        else {
            String getMgrSting = getMagegerIds(mrgNo);
            selectQuery = "SELECT  * FROM " + TABLE_EMPLOYEE_LIST + " Where " + KEY_EMP_MARGNO + "='" + mrgNo + "' or " + KEY_EMP_MARGNO + " in" + getMgrSting + " order by " + sortBy;

        }

        ArrayList<EmployeeInfo> mobjreturn = new ArrayList<>();

        SQLiteDatabase db = this.getReadableDatabase();
        try {
            Cursor cursor = db.rawQuery(selectQuery, null);

            if (cursor.moveToFirst()) {

                do {
                    EmployeeInfo empObj = new EmployeeInfo(cursor.getString(1), cursor.getString(2), cursor.getString(3), cursor.getString(4), cursor.getString(5));

                    mobjreturn.add(empObj);
                } while (cursor.moveToNext());

            }
            cursor.close();
            db.close();

        } catch (Exception ex) {
            String str = ex + "";
        }

        return mobjreturn;
    }


    private String getMagegerIds(String mrgNo) {


        String returnMgrIds = "('";
        ArrayList<String> mrgIds = new ArrayList<>();
        try {


            String selectQuery = "";
            SQLiteDatabase db = this.getReadableDatabase();
            ArrayList<String> newMgrList = new ArrayList<>();

            selectQuery = "SELECT emp_no FROM " + TABLE_EMPLOYEE_LIST + " Where " + KEY_EMP_MARGNO + "='" + mrgNo + "' and " + KEY_IS_MRG + "='Y'";
            Cursor cursor = db.rawQuery(selectQuery, null);
            if (cursor.moveToFirst()) {

                do {

                    mrgIds.add(cursor.getString(0));
                    newMgrList.add(cursor.getString(0));
                } while (cursor.moveToNext());

            }
            cursor.close();
            if (newMgrList.size() == 0) {
                returnMgrIds = "('')";
                return returnMgrIds;
            }

            while (newMgrList.size() != 0) {
                String strInPara = "('";
                for (int i = 0; i < newMgrList.size(); i++) {
                    if (i < (newMgrList.size() - 1)) {
                        strInPara += newMgrList.get(i) + "','";
                    } else {
                        strInPara += newMgrList.get(i) + "')";
                    }
                }
                newMgrList.clear();
                selectQuery = "SELECT  emp_no FROM " + TABLE_EMPLOYEE_LIST + " Where " + KEY_EMP_MARGNO + " in" + strInPara + " and " + KEY_IS_MRG + "='Y'";
                cursor = db.rawQuery(selectQuery, null);
                if (cursor.moveToFirst()) {

                    do {

                        newMgrList.add(cursor.getString(0));
                    } while (cursor.moveToNext());

                }
                if (newMgrList.size() != 0) {
                    mrgIds.addAll(newMgrList);
                }
                cursor.close();
            }
            for (int i = 0; i < mrgIds.size(); i++) {
                if (i < (mrgIds.size() - 1)) {
                    returnMgrIds += mrgIds.get(i) + "','";
                } else {
                    returnMgrIds += mrgIds.get(i) + "')";
                }
            }
            if (db.isOpen())
                db.close();


        } catch (Exception ex) {
            String str = ex + "";
            returnMgrIds = "('')";
        }
        return returnMgrIds;
    }*/


}
