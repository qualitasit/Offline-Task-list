package list.task.offline.offlinetasklist;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.content.LocalBroadcastManager;


import java.util.HashMap;
import java.util.Map;

/**
 * Created by user on 25-07-16.
 */
public class CompanyPreferences {
    private static String TAG = CompanyPreferences.class.getSimpleName();

    Context _context;



    private static final CompanyPreferences INSTANCE = new CompanyPreferences();
    private static final String PREF_NAME = "CompanyPreferences";
    private static SharedPreferences pref;
    private static SharedPreferences.Editor editor;
    public static final String FRUIT_SYNCED_ID = "Fruit_id";
    public static CompanyPreferences getInstance() {
        if (pref == null || editor == null) {
            pref = AppController.getAppContext().getSharedPreferences(PREF_NAME, 0);
            editor = pref.edit();
        }

        return INSTANCE;
    }

    private CompanyPreferences() {

    }
    public void setFruitSynId(int id)
    {
        editor.putInt(FRUIT_SYNCED_ID, id);
        editor.commit();
    }
    public int getFruitSynId()
    {
        return pref.getInt(FRUIT_SYNCED_ID, 1);

    }

}
